﻿using LaSalleBajio.Practica1.Core.Dto;
using Microsoft.AspNetCore.Mvc;

namespace LaSalleBajio.Practica1.Api.Services.Interfaces;

    public interface IHeroService
    {
    public List<HeroeDto> ObtenerTodoService();
    public List<HeroeDto> ObtenerTipoHeroe(string type);


}

