﻿using LaSalleBajio.Practica1.Api.Repositories.Interfaces;
using LaSalleBajio.Practica1.Api.Services.Interfaces;
using LaSalleBajio.Practica1.Core.Dto;

namespace LaSalleBajio.Practica1.Api.Services;

public class HeroeService : IHeroService
{

    private readonly IHeroRepository _repository;

    //inyeccion en constructor
    public HeroeService(IHeroRepository repository)
    {
        _repository = repository;
     }

    //todos
    public List<HeroeDto> ObtenerTodoService()
    {
        var sistem = _repository.ObtieneTodoRepository();

        return sistem.Select(c=>new HeroeDto(c)).ToList();
    }

    public List<HeroeDto> ObtenerTipoHeroe(string type)
    {
        var sistem = _repository.ObtieneTipoHeroeRepository(type);

        return sistem.Select(c => new HeroeDto(c)).ToList();
    }
}

