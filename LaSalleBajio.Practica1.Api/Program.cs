using LaSalleBajio.Practica1.Api.DataAcces;
using LaSalleBajio.Practica1.Api.Repositories;
using LaSalleBajio.Practica1.Api.Repositories.Interfaces;
using LaSalleBajio.Practica1.Api.Services;
using LaSalleBajio.Practica1.Api.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



builder.Services.AddScoped<IHeroRepository, HeroeRepository>();
builder.Services.AddScoped<IHeroService, HeroeService>();

//******************************
//CONFIGURACION DE LOS CORS
//*****************************
builder.Services.AddCors(x => x.AddPolicy("EnableCors", builder => {
    builder.SetIsOriginAllowedToAllowWildcardSubdomains()
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader();
    }));


/**
 * En .NET 6, los servicios como el contexto de base de datos deben 
 * registrarse con el contenedor de inyeccin de dependencia (DI) . 
 * El contenedor proporciona el servicio a los controladores. * 
 */

builder.Services.AddDbContext<DbContextMobile>(options => options.UseSqlServer(
    builder.Configuration.GetConnectionString("DefaultConnection")
    ));



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//Permitir los CORES
app.UseCors("EnableCors");

app.UseAuthorization();

app.MapControllers();

app.Run();
