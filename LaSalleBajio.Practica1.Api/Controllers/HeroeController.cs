﻿using LaSalleBajio.Practica1.Api.Services.Interfaces;
using LaSalleBajio.Practica1.Core.Dto;
using LaSalleBajio.Practica1.Core.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;

namespace LaSalleBajio.Practica1.Api.Controllers;



[ApiController]
[Route("api/[controller]")]
public class HeroeController : ControllerBase
{
    //declaramos servicios   
    private readonly IHeroService _service;

    //inyectamos servicio
    public HeroeController(IHeroService service)
    {
        _service = service;
    }

    //1
    [HttpGet]
    public ActionResult<Response<List<HeroeDto>>> GetAll()
    {


        //Llenamos la propiedad data del response
        var response = new Response<List<HeroeDto>>()
        {
            Data = _service.ObtenerTodoService()
        };


        return Ok(response);


    }



    //2
    [HttpGet("{type}")]
    public ActionResult<Response<List<HeroeDto>>> GetTypeHero(string type)
    {


        //Llenamos la propiedad data del response
        var response = new Response<List<HeroeDto>>()
        {
            Data = _service.ObtenerTipoHeroe(type)
        };


        return Ok(response);


    }

}
   


