﻿using LaSalleBajio.Practica1.Api.DataAcces;

using LaSalleBajio.Practica1.Api.Repositories.Interfaces;
using LaSalleBajio.Practica1.Core.Entities;

namespace LaSalleBajio.Practica1.Api.Repositories;

public class HeroeRepository: IHeroRepository
{
    //dbContext
    private readonly DbContextMobile _dbContext;

    

    //inyectamos el contexto al constructor
    public HeroeRepository(DbContextMobile dbContext)
    {
        _dbContext = dbContext; 

    }


    /***
     *Metodos
     **/
    
    public List<Heroe> ObtieneTodoRepository()
    {

        //return _db.K_CXC.Where(cxc => cxc.NO_CXC == nocxc).ToList();      
        return _dbContext.heroes.ToList();
    }
           
    public List<Heroe> ObtieneTipoHeroeRepository(string type)
    {

        return _dbContext.heroes.Where(hero => hero.publisher == type).ToList();      
       // return _dbContext.heroes.ToList();
    }


}
