﻿
using LaSalleBajio.Practica1.Core.Entities;

namespace LaSalleBajio.Practica1.Api.Repositories.Interfaces;

    public interface IHeroRepository
    {
        public List<Heroe> ObtieneTodoRepository();
        public List<Heroe> ObtieneTipoHeroeRepository(string type);
    }

