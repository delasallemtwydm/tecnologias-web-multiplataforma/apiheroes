﻿
using LaSalleBajio.Practica1.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace LaSalleBajio.Practica1.Api.DataAcces
{
    public class DbContextMobile : DbContext
    {
        public DbContextMobile(DbContextOptions<DbContextMobile> options):base(options)
        {
          
        }

        /*
        * Configurar esquemas
        */
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //multiples esquemas            
            //modelBuilder.Entity<E_Cartera>().ToTable("ClasificacionCartera", "dbo");
        }


        //DbSet<Sistema> esta es la clase
        //sistemas debe llamarse igual que la tabla
        public DbSet<Heroe> heroes { get; set; } = null!;

       
    }
}
