﻿using System.ComponentModel.DataAnnotations;

namespace LaSalleBajio.Practica1.Core.Entities;

public class Heroe : EntityBase
{
    [Key]
    public int Id { get; set; }
    public string superhero { get; set; }
    public string publisher { get; set; }
    public string alter_eg { get; set; }
    public string first_appearance { get; set; }
    public string characters { get; set; }
    public string image { get; set; }
}


