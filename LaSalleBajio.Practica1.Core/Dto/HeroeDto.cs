﻿using LaSalleBajio.Practica1.Core.Entities;

namespace LaSalleBajio.Practica1.Core.Dto;
public class HeroeDto : DtoBase
{
    public string superhero { get; set; }
    public string publisher { get; set; }
    public string alter_eg { get; set; }
    public string firs_apparence { get; set; }
    public string characters { get; set; }
    public string imagen { get; set; }

    public HeroeDto()
	{}

	public HeroeDto(Heroe heroes)
	{
		Id = heroes.Id;
		superhero = heroes.superhero;
        publisher= heroes.publisher;
        alter_eg=heroes.alter_eg;
        firs_apparence = heroes.first_appearance;
        characters=heroes.characters;
        imagen = heroes.image;
			
	}

}
